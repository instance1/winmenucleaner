# WinMenuCleaner



Tired of installers cluttering up your desktop and inserting themselves in the root of your start
menu? So was I.

WinMenuCleaner runs as a phar in an administrative PowerShell and moves everything
to where you want.

It runs off a simple JSON file and supports these operations:
- delete,
- move,
- move to a sub-folder
- run a command

It also supports wildcards, so when a package name incorporates a version number,
you don't have to change your configuration to match. Here's a sample configuration file:

```json
{
  "paths": {
    "app-menu": "C:\\Users\\me\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\",
    "programs": "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\",
    "pub-desk": "C:\\Users\\Public\\Desktop\\",
    "start": "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\",
    "user-desktop": "C:\\Users\\me\\Desktop\\"
  },
  "fixes": [
    {
      "name": "7-Zip",
      "actions": [
        {
          "source": "<programs>7-Zip",
          "op": "sub",
          "relPath": "System"
        }
      ]
    },
    {
      "name": "Acrobat Reader",
      "actions": [
        {
          "source": "<pub-desk>Acrobat Reader DC.lnk",
          "op": "delete"
        },
        {
          "source": "<programs>Acrobat Reader DC.lnk",
          "op": "sub",
          "relPath": "Crap\\Adobe"
        }
      ]
    },
    {
      "name": "LibreOffice",
      "actions": [
        {
          "source": "<pub-desk>LibreOffice *.lnk",
          "op": "delete"
        },
        {
          "sourceMatch": "<programs>LibreOffice*",
          "op": "sub",
          "relPath": "Office"
        }
      ]
    },
    {
      "name": "SomeApp",
      "actions": [
        {
          "source": "<start>SomeFolder",
          "op": "move",
          "target": "<app-menu>Development"
        },
        {
          "op": "run",
          "cmd": "service restart someApp"
        }
      ]
    }
  ]
}
```

The `paths` section defines common locations where installers place shortcuts and desktop icons.
Use `<path-name>` in a source ot target path to get the path expended.

Each entry in `fixes` has a name and a list of actions. Most actions will use the `sub` command to 
move matching files into a sub-folder. The `delete` command is useful for removing unwanted desktop
icons. In some relatively infrequent cases applications will install into locations that cause
duplicate entries on the start menu. The `move` command handles that case.

Single source files are identified with the `source` property. Multiple files are specified 
with the `sourceMatch`, which uses file wildcard characters ? and *.

If you're using a package manager like the excellent Chocolatey, then a WinMenuCleaner
run immediately after an update will ensure that updated shortcuts are moved to your preferred
location.

WinMenuCleaner expects a path to the configuration JSON file on the command line. If none is
provided it will look for `cleaner.json` in the current directory.

## Running WinMenuCleaner

### Using the PHAR file

All you need to run is the WinMenuCleaner.phar file and an installed PHP (7.4 or 8.0). Open an
administrative shell and execute

```
php WinMenuCleaner.phar [config-file]
```

### Using the source

Clone the repository, run `composer install` and run `src/index.php`.

### Building the PHAR file

Run `compiler.php` in the project root.