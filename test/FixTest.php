<?php


use PHPUnit\Framework\TestCase;

class FixTest extends TestCase
{

    public function testRun()
    {
        $action = $this->createMock(Action::class);
        $action->expects($this->once())
            ->method('run')
            ->willReturn(true);

        $obj = new Fix();
        $obj->addAction($action);
        $obj->run();
    }
}
