<?php


use PHPUnit\Framework\TestCase;

class CleanerTest extends TestCase
{
    protected string $json = '
        {
            "paths": {
                "path1": "path\/to\/one\/",
                "path2": "path\/to\/two\/"
            },
            "fixes": [
                {
                    "name": "fix1",
                    "actions": [
                        {
                            "source": "<path1>\/part\/somefile.txt",
                            "op": "delete"
                        }
                    ]
                }
            ]
        }
        ';

    public function testClean()
    {
        $fix = $this->createMock(Fix::class);
        $fix->expects($this->once())
            ->method('run');
        $obj = new Cleaner();
        $obj->addFix($fix);
        $obj->clean();
    }

    public function testLoad()
    {
        $obj = new Cleaner();
        $obj->load($this->json);
        $this->assertEquals('path/to/one/foo', $obj->resolve('<path1>foo'));
    }

    public function testResolve()
    {
        $obj = new Cleaner();
        $obj->addPath('foo', 'foo/bar/');
        $this->assertEquals('foo/bar/baz', $obj->resolve('<foo>baz'));
    }
}
