<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ActionTest extends TestCase
{

    public function testRunCopy()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');

        $zot = Filesystem::class;
        $mockFS = $this->createStub(Filesystem::class);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dira/testfile.new', true, 'dira/testfile.new'],
                ['dirb/testfile.txt', true, 'dirb/testfile.txt'],
            ]);
        $mockFS->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('dirb/testfile.txt'))
            ->willReturn(true);
        $mockFS->expects($this->once())
            ->method('copy')
            ->with(
                $this->equalTo('dirb/testfile.txt'),
                $this->equalTo('dira/testfile.new'),
                true
            )
            ->willReturn(true);

        $json = '{"source":"<b>testfile.txt","op":"copy","target":"<a>testfile.new"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunDelete()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');

        $mockFS = $this->createMock(Filesystem::class);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dirb/testfile.txt', true, 'dirb/testfile.txt'],
            ]);
        $mockFS->expects($this->once())
            ->method('remove')
            ->with($this->equalTo('dirb/testfile.txt'))
            ->willReturn(true);

        $json = '{"source":"<b>testfile.txt","op":"delete"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunDeleteMatch()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');

        $mockFS = $this->createMock(Filesystem::class);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dira/myapp *', true, 'dira/myapp *'],
                ['dirb', true, 'dirb'],
                ['dirb/', true, 'dirb']
            ]);
        $folderA = ['myapp 1.0', 'myapp 2.0', 'myapp 2.1'];
        $folderB = ['testfile.txt', 'someotherfile.txt'];
        $mockFS->method('scanDir')
            ->willReturnMap([
                ['dira', false, $folderA],
                ['dira', true, $folderA],
                ['dirb', false, $folderB],
                ['dirb', true, $folderB],
            ]);
        $mockFS->expects($this->exactly(3))
            ->method('remove')
            ->withConsecutive(
                [$this->equalTo('dira/myapp 1.0')],
                [$this->equalTo('dira/myapp 2.0')],
                [$this->equalTo('dira/myapp 2.1')]
            )
            ->willReturn(true);

        $json = '{"sourceMatch":"<a>myapp *","op":"delete"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunMove()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');

        $mockFS = $this->createMock(Filesystem::class);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dirb/testfile.txt', true, 'dirb/testfile.txt']
            ]);
        $mockFS->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('dirb/testfile.txt'))
            ->willReturn(true);
        $mockFS->expects($this->once())
            ->method('rename')
            ->with(
                $this->equalTo('dirb/testfile.txt'),
                $this->equalTo('dira/testfile.txt'),
                true
            )
            ->willReturn(true);

        $json = '{"source":"<b>testfile.txt","op":"move","target":"<a>"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunMoveWild()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');

        $mockFS = $this->createMock(Filesystem::class);
        $folderA = ['myapp 1.0', 'myapp 2.0', 'myapp 2.1'];
        $folderB = ['testfile.txt', 'someotherfile.txt'];
        $mockFS->method('scanDir')
            ->willReturnMap([
                ['dira', false, $folderA],
                ['dira', true, $folderA],
                ['dirb', false, $folderB],
                ['dirb', true, $folderB],
            ]);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dira/myapp*', true, 'dira/myapp*'],
                ['dirb', true, 'dirb'],
                ['dirb/', true, 'dirb']
            ]);
        $mockFS->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('dira'))
            ->willReturn(true);
        $mockFS->expects($this->atLeastOnce())
            ->method('move')
            ->withConsecutive(
                [$this->equalTo('dira/myapp 1.0'), $this->equalTo('dirb/myapp 1.0')],
                [$this->equalTo('dira/myapp 2.0'), $this->equalTo('dirb/myapp 2.0')],
                [$this->equalTo('dira/myapp 2.1'), $this->equalTo('dirb/myapp 2.1')]
            )
            ->willReturn(true);

        $json = '{"sourceMatch":"<a>myapp*","op":"move","target":"<b>"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunSub()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');
        $cleaner->addPath('s', '/sub');

        $mockFS = $this->createMock(Filesystem::class);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dirb/testfile.txt', true, 'dirb/testfile.txt'],
                ['dirb//sub', true, 'dirb/sub']
            ]);
        $mockFS->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('dirb/testfile.txt'))
            ->willReturn(true);
        $mockFS->expects($this->once())
            ->method('rename')
            ->with(
                $this->equalTo('dirb/testfile.txt'),
                $this->equalTo('dirb/sub/testfile.txt'),
                true
            )
            ->willReturn(true);

        $json = '{"source":"<b>testfile.txt","op":"sub","relPath":"<s>"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunSubWild()
    {
        $cleaner = new Cleaner();
        $cleaner->addPath('a', 'dira/');
        $cleaner->addPath('b', 'dirb/');
        $cleaner->addPath('s', '/sub');

        $mockFS = $this->createMock(Filesystem::class);
        $folderA = ['myapp 1.0', 'myapp 2.0', 'myapp 2.1'];
        $folderB = ['testfile.txt', 'someotherfile.txt'];
        $mockFS->method('scanDir')
            ->willReturnMap([
                ['dira', false, $folderA],
                ['dira', true, $folderA],
                ['dirb', false, $folderB],
                ['dirb', true, $folderB],
            ]);
        $mockFS->expects($this->atLeastOnce())
            ->method('cleanPath')
            ->willReturnMap([
                ['dira', true, 'dira'],
                ['dira/', true, 'dira'],
                ['dira/myapp*', true, 'dira/myapp*'],
                ['dira//sub', true, 'dira/sub'],
                ['dirb', true, 'dirb'],
                ['dirb/', true, 'dirb']
            ]);
        $mockFS->expects($this->once())
            ->method('exists')
            ->with($this->equalTo('dira'))
            ->willReturn(true);
        $mockFS->expects($this->atLeastOnce())
            ->method('move')
            ->withConsecutive(
                [$this->equalTo('dira/myapp 1.0'), $this->equalTo('dira/sub/myapp 1.0')],
                [$this->equalTo('dira/myapp 2.0'), $this->equalTo('dira/sub/myapp 2.0')],
                [$this->equalTo('dira/myapp 2.1'), $this->equalTo('dira/sub/myapp 2.1')]
            )
            ->willReturn(true);

        $json = '{"sourceMatch":"<a>myapp*","op":"sub","relPath":"<s>"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

    public function testRunRun()
    {
        $cleaner = new Cleaner();
        $mockFS = $this->createMock(Filesystem::class);
        $mockFS->expects($this->once())
            ->method('exec')
            ->with($this->equalTo('ls'))
            ->willReturn(0);

        $json = '{"op":"run","cmd":"ls"}';
        $obj = new Action($mockFS, $cleaner);
        $obj->hydrate($json);
        $obj->run();
    }

}
