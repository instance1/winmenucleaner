<?php
declare(strict_types=1);

use Abivia\Hydration\Hydratable;
use Abivia\Hydration\HydrationException;
use Abivia\Hydration\Hydrator;
use Abivia\Hydration\Property;
use Symfony\Component\Filesystem\Exception\IOException;

class Action implements Hydratable
{
    private ?Cleaner $cleaner = null;

    protected array $cmd = [];

    private Filesystem $filesystem;

    private static Hydrator $hydrate;
    protected string $op = '';
    protected ?string $relPath = null;
    protected ?string $source = null;
    protected ?string $sourceMatch = null;
    protected ?string $target = null;

    public function __construct(
        ?Filesystem $filesystem = null,
        ?Cleaner $cleaner = null
    ) {
        $this->cleaner = $cleaner;
        $this->filesystem = $filesystem ?? new Filesystem();
    }

    /**
     * Copy a file or folder.
     * @param string $targetPath
     */
    private function copy(string $targetPath)
    {
        if (isset($this->source)) {
            $source = $this->cleaner->resolve($this->source);
            $source = $this->filesystem->cleanPath($source);
            if ($this->filesystem->exists($source)) {
                $target = $this->cleaner->resolve($targetPath);
                $target = $this->filesystem->cleanPath($target);
                $this->cleaner->debug("Copy $source to $target");
                $this->filesystem->copy(
                    $source, "$target", true
                );
            }
        }
    }

    private function delete()
    {
        if ($this->sourceMatch !== null) {
            $source = $this->cleaner->resolve($this->sourceMatch);
            $source = $this->filesystem->cleanPath($source);
            $dirName = dirname($source);
            $pattern = substr($source, strlen($dirName) + 1);
            $scan = $this->filesystem->scanDir($dirName);
            foreach ($scan as $file) {
                if (fnmatch($pattern, $file)) {
                    $this->cleaner->debug("Delete $dirName/$file");
                    $this->filesystem->remove("$dirName/$file");
                }
            }
        }
        if ($this->source !== null) {
            $source = $this->cleaner->resolve($this->source);
            $source = $this->filesystem->cleanPath($source);
            $this->cleaner->debug("Delete $source");
            $this->filesystem->remove($source);
        }
    }

    /**
     * @inheritDoc
     */
    public function hydrate($config, ?array $options = []): bool
    {
        if (!isset(self::$hydrate)) {
            self::$hydrate = Hydrator::make()
                ->addProperty(
                    Property::make('op')->validate(function ($value): bool {
                        return in_array(
                            $value,
                            ['copy', 'delete', 'move', 'run', 'sub']
                        );
                    })
                    ->require()
                )
                ->addProperty(
                    Property::make('cmd')->key()
                )
                ->addProperties(['relPath', 'source', 'sourceMatch', 'target'])
                ->bind(self::class, 0);
        }
        $this->cleaner ??= $options['_Cleaner'];

        $result = self::$hydrate->hydrate($this, $config, $options);
        if ($result) {
            // Extended validation
            if ($this->op === 'copy' && $this->source === null) {
                throw new HydrationException(
                    "Copy operation requires a source property."
                );
            } elseif (
                $this->op !== 'run'
                && $this->source === null
                && $this->sourceMatch === null
            ) {
                throw new HydrationException(
                    "Either source or sourceMatch property must be provided."
                );
            }
            if (in_array($this->op, ['sub']) && $this->relPath === null) {
                throw new HydrationException(
                    "'sub' operation must supply a relPath."
                );
            }
            if (in_array($this->op, ['copy', 'move']) && $this->target === null) {
                throw new HydrationException(
                    "'copy' and 'move' operations must supply a target."
                );
            }
        }

        return $result;
    }

    /**
     * Move a file or folder, merging into the target.
     * @param string $targetDir
     */
    private function move(string $targetDir)
    {
        if (isset($this->sourceMatch)) {
            $this->moveWildcard($targetDir);
        }
        if (isset($this->source)) {
            $source = $this->cleaner->resolve($this->source);
            $source = $this->filesystem->cleanPath($source);
            if ($this->filesystem->exists($source)) {
                $target = $this->cleaner->resolve($targetDir);
                $target = $this->filesystem->cleanPath($target);
                $this->cleaner->debug("Move $source to $target");
                $this->filesystem->rename(
                    $source, "$target/" . basename($source), true
                );
            }
        }
    }

    /**
     * Move a file or folder to a sub-folder, merging into the target.
     * @return bool
     */
    private function moveToSubFolder(): void
    {
        if (isset($this->sourceMatch)) {
            $source = $this->cleaner->resolve($this->sourceMatch);
            $source = $this->filesystem->cleanPath($source);
            $dirName = dirname($source);
            $this->move("$dirName/$this->relPath");
        }
        if (isset($this->source)) {
            $source = $this->cleaner->resolve($this->source);
            $source = $this->filesystem->cleanPath($source);
            $subPath = dirname($source) . "/$this->relPath";
            $this->move($subPath);
        }
    }

    private function moveWildcard(string $targetDir)
    {
        $source = $this->cleaner->resolve($this->sourceMatch);
        $source = $this->filesystem->cleanPath($source);
        $dirName = dirname($source);
        if (!$this->filesystem->exists($dirName)) {
            return;
        }
        $pattern = substr($source, strlen($dirName) + 1);
        $scan = $this->filesystem->scanDir($dirName);
        $target = $this->cleaner->resolve($targetDir);
        $target = $this->filesystem->cleanPath($target);
        foreach ($scan as $file) {
            if (fnmatch($pattern, $file)) {
                $this->cleaner->debug("Move $dirName/$file to $target/$file");
                $this->filesystem->move("$dirName/$file", "$target/$file");
            }
        }
    }

    public function op() {
        return $this->op;
    }

    public function run()
    {
        $result = false;
        switch ($this->op) {
            case 'copy':
                $this->copy($this->target);
                break;
            case 'delete':
                $this->delete();
                break;
            case 'move':
                $this->move($this->target);
                break;
            case 'run':
                $this->runCmd();
                break;
            case 'sub':
                $this->moveToSubFolder();
        }
    }

    protected function runCmd()
    {
        foreach ($this->cmd as $cmd) {
            $result = $this->filesystem->exec($cmd);
            if ($result !== 0) {
                echo "returns $result: $cmd";
                break;
            }
        }
    }

}
