<?php
declare(strict_types=1);

use Abivia\Hydration\Hydratable;
use Abivia\Hydration\Hydrator;
use Abivia\Hydration\Property;

class Fix implements Hydratable
{
    /**
     * @var Action[]
     */
    private array $actions = [];

    private string $name = '';

    private static Hydrator $hydrate;

    public function addAction(Action $action)
    {
        $this->actions[] = $action;
    }

    /**
     * @inheritdoc
     * @throws ReflectionException
     */
    public function hydrate($config, ?array $options = []): bool
    {
        if (!isset(self::$hydrate)) {
            self::$hydrate = Hydrator::make()
                ->addProperties(['name'])
                ->addProperty(
                    Property::make('actions')->key()->bind(Action::class)
                )
                ->bind(self::class);
        }

        return self::$hydrate->hydrate($this, $config, $options);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function run(): void
    {
        foreach ($this->actions as $action) {
            $action->run();
        }
    }

}
