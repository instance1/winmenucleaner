<?php
declare(strict_types=1);

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem as SymFS;

class Filesystem extends SymFS
{
    private static array $dirCache = [];

    /**
     * Make sure a directory exists
     * @param string $dir
     * @throws IOException
     */
    public function checkDir(string $dir)
    {
        if (!is_dir($dir)) {
            $this->mkdir($dir);
        }
    }

    /**
     * @param string $path
     * @param bool $posix
     * @return string
     */
    public function cleanPath(string $path, $posix = true): string
    {
        if ($posix) {
            $path = preg_replace('![/\\\\]+!', '/', $path);
            $path = rtrim($path, '/');
        } else {
            $path = preg_replace('![/\\\\]+!', '\\', $path);
            $path = rtrim($path, '\\');
        }

        return $path;
    }

    public function exec(string $cmd): int
    {
        exec($cmd, $dummy, $result);
        return $result;
    }

    /**
     * Move the source to the target, overwriting if required.
     * @param string $source
     * @param string $target
     * @throws IOException
     */
    public function move(string $source, string $target)
    {
        $this->checkDir(dirname($target));
        $this->rename($source, $target, true);
    }

    /**
     * Get and cache a list of files in a directory (exclusive of current and parent).
     * @param string $dirName
     * @param bool $cacheBust True to flush cache first.
     * @return array
     * @throws IOException
     */
    public function scanDir(string $dirName, bool $cacheBust = false)
    {
        if ($cacheBust || !isset(self::$dirCache[$dirName])) {
            $dir = @scandir($dirName);
            if ($dir === false) {
                throw new IOException("'$dirName' is not a directory.");
            }
            foreach($dir as $key => $file) {
                if ($file === '.' || $file === '..' || strtolower($file) === 'desktop.ini') {
                    unset($dir[$key]);
                }
            }
            self::$dirCache[$dirName] = array_values($dir);
        }

        return self::$dirCache[$dirName];
    }

}
