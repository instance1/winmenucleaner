<?php

require_once '../vendor/autoload.php';

$args = $argv;
// Lose the script name
array_shift($args);

// Look for debug mode
$debugging = false;
foreach ($args as $key => $value) {
    if ($value === '--debug') {
        $debugging = true;
        unset($args[$key]);
    }
}
$configPath = count($args) ? array_shift($args) : getcwd() . "/cleaner.json";

$fs = new Filesystem();
$configPath = $fs->cleanPath($configPath, false);
if (!file_exists($configPath)) {
    echo "Configuration file $configPath not found.\n\n";
}
if (count($args)) {
    echo "Usage: WinMenuCleaner [options|config_file]\n"
        . "Default config file is cleaner.json\n"
        . "Option: --debug\n\n";
    exit(1);
}

$o = new Cleaner();
if ($debugging) {
    $o->setDebug(true);
}

$o->load($configPath);
$o->clean();
