<?php
declare(strict_types=1);

use Abivia\Hydration\Hydratable;
use Abivia\Hydration\HydrationException;
use Abivia\Hydration\Hydrator;
use Abivia\Hydration\Property;

/**
 * Clean up things left in places we don't like by overzealous installers.
 */
class Cleaner implements Hydratable
{
    /**
     * @var bool Debugging flag
     */
    private bool $debug = false;

    /**
     * @var Fix[] A list of fixes to be made to clean up.
     */
    private array $fixes = [];

    /**
     * @var string[] Path substitutions, indexed by <sourcePath>
     */
    private array $paths = [];

    public function addFix(Fix $fix)
    {
        $this->fixes[$fix->name()] = $fix;
    }

    public function addPath(string $key, string $path)
    {
        $this->paths["<$key>"] = $path;
    }

    /**
     * Perform the clean-up.
     */
    public function clean() {
        try {
            $fixNumber = 0;
            foreach ($this->fixes as $fix) {
                ++$fixNumber;
                $fix->run();
            }
        } catch (Exception $ex) {
            echo "Fail on " . $fix->name() . " due to\n" . $ex->getMessage() . "\n";
            if ($this->debug) {
                echo  " at " . $ex->getFile() . ':' . $ex->getLine() . "\n";
                $trace = $ex->getTraceAsString();
                $trace = preg_replace('!(c|d|phar):.*?src/!', '', $trace);
                echo $trace;
            }
        }
    }

    public function debug(string $msg)
    {
        if ($this->debug) {
            echo "$msg\n";
        }
    }

    /**
     * @inheritdoc
     */
    public function hydrate($config, ?array $options = []): bool
    {
        $hydrate = Hydrator::make()
            ->addProperty(
                Property::make('fixes')
                    ->key('name')
                    ->bind(Fix::class)
            )
            ->addProperty(
                Property::make('paths')
                    ->toArray()
            )
            ->bind(self::class)
        ;
        $options['_Cleaner'] = $this;

        $result = $hydrate->hydrate($this, $config, $options);
        if ($result) {
            $this->loadPaths();
        }

        return $result;
    }

    /**
     * Load a configuration from a file.
     *
     * @param string $configPath The JSON list of fixes to apply.
     */
    public function load(string $configPath)
    {
        try {
            if ($fromFile = file_exists($configPath)) {
                $config = file_get_contents($configPath);
            } else {
                $config = $configPath;
            }
            $this->hydrate($config);

        } catch (Exception $ex) {
            echo "Error while loading " . ($fromFile ? $configPath : 'json')
                . "\n" . $ex->getMessage();
        }
    }

    /**
     * Add angle brackets around the defined paths to facilitate replacement.
     */
    private function loadPaths()
    {
        $bracketed = [];
        foreach ($this->paths as $purpose => $path) {
            $bracketed["<$purpose>"] = $path;
        }
        $this->paths = $bracketed;
    }

    /**
     * Apply path substitutions to a path.
     *
     * @param string $path
     * @return string
     */
    public function resolve(string $path): string
    {
        return str_replace(array_keys($this->paths), $this->paths, $path);
    }

    public function setDebug(bool $flag): self
    {
        $this->debug = $flag;
        return $this;
    }

}
